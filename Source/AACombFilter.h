/*
 *	File:		AACombFilter.h
 *
 *	Version:	1.0
 *
 *	Created:	12-10-24 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#include "AADelay.h"

// for the comb filter low-pass filter
#include "AAFilter.h"
#include "AAIIRFilter.h"

#ifndef _AACombFilter_h_
#define _AACombFilter_h_

using namespace AADelay;

const double DEFAULT_LPFILTER_FREQ	= 10000.;


#pragma mark ____AACombFilter Declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	A low-pass feedback comb filter
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class AACombFilter : public AADelayBase {
    
private:
    
    AAIIRFilter aAdelayLpFilter_;
	FilterParams aAdelayLpFilterParams_;
    
    double      aAdelayGain_;
    
public:
	
	AACombFilter		();
	AACombFilter		(double dTime, double rvt, const float sr=DEFAULT_SAMPLE_RATE);
	~AACombFilter       ();
	
	void	setCombFilterGain   (double dTime, double rvt, const float sr=DEFAULT_SAMPLE_RATE);
	void	setCombFilterLpFreq	(const double lpFreq);
	
	double	getCombFilterGain	()		const		{ return aAdelayGain_; }
	double	getCombFilterLpFreq	()		const		{ return aAdelayLpFilter_.getFrequency(); }
	
	// fills output with the delay tail and returns the length (number of samples) of the tail
	uint	flush				(float *output);
	uint	flush				(double *output);
    
	void	process				(float *input, float *output, const uint samples);
	void	process				(double *input, double *output, const uint samples);
	
};		// AACombFilter


#endif // _AACombFilter_h_

