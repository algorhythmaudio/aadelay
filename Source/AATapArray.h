/*
 *	File:		AATapArray.h
 *
 *	Version:	1.0
 *
 *	Created:	12-10-22 by Christian Floisand
 *	Updated:	12-10-23 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	AATapArray defines a class that holds a series of taps for use in AATapDelay.  
 *	Each tap contains an offset value, which is related to the delay time, and specifies 
 *	the offset in samples in the circular buffer as well as the corresponding gain value.
 *
 *	@usage:--
 *		AATapArray tapArray(taps, times[], gains[], sr);
 *			taps is the number of taps in the two arrays times[] and gains[]
 *			where times[] is an array of delay time values and gains[] is the
 *			corresponding array of gain values for each tap.
 *		AATapArray newTapArray(tapArray);
 *			is the copy constructor, and copies all tap information from tapArray 
 *			into newTapArray.
 *			Similarly, the assignment operator can be used to copy a tap array.
 *		newTapArray.add_tap(time, gain);
 *			adds a tap to the array with a delay time and gain value specified.
 *		tapArray.delete_tap(tap);
 *			deletes a tap from the array where tap is >= 0 and < num_taps (0 is the first
 *			tap and num_taps-1 is the last tap).
 *		newTapArray.modify_tap_time(tap, time);
 *		tapArray.modify_tap_gain(tap, gain);
 *			modifies delay time/gain value of the tap in the array specified by tap, 
 *			where tap is >= 0 and < num_taps.
 *
 *		Sample rate must be accurate in order for the offset values to be correct 
 *		in processing, so use set_samplerate(sr) if it changes.
 *		Use get_offset_at(tap)/get_gain_at(tap) to retrieve tap information.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#include <iostream>
#include <cstring>
#include <vector>

#ifndef _AATapArray_h_
#define _AATapArray_h_

const uint MAX_NUM_TAPS			= 10;		// maximum number of delay taps


#pragma mark ____AATapArray Declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapArray class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class AATapArray {
    
public:
	
	/* constructors/destructor
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	AATapArray		()
	{
		num_taps_ = 0;
        max_offset_ = 0;
        samplerate_ = DEFAULT_SAMPLE_RATE;
	}
    
    AATapArray  (const uint n_taps, double *dTimes, double *dGains, const float sr=DEFAULT_SAMPLE_RATE)
    {
		num_taps_ = (n_taps > MAX_NUM_TAPS ? MAX_NUM_TAPS : n_taps);
        samplerate_ = sr;
        max_offset_ = 0;
		
		validate_delay_times(dTimes);
		validate_delay_gains(dGains);
        
        uint *temp_offsets = new uint[num_taps_];
        
        for (int i = 0; i < num_taps_; ++i) {
            temp_offsets[i] = static_cast<uint>(dTimes[i] * samplerate_);
            max_offset_ = (max_offset_ > temp_offsets[i] ? max_offset_ : temp_offsets[i]);
        }
		
        offsets_.assign(temp_offsets, temp_offsets+num_taps_);
        
        delete[] temp_offsets;
        
        for (int i = 0; i < num_taps_; ++i)
            offsets_[i] = max_offset_ - offsets_[i];
        
        gains_.assign(dGains, dGains+num_taps_);
    }
	
	AATapArray (const AATapArray& tap_array, const float sr=DEFAULT_SAMPLE_RATE)
	{
        num_taps_ = tap_array.get_num_taps();
		max_offset_ = tap_array.get_max_offset();
		samplerate_ = tap_array.get_samplerate();
		
		uint *temp_offsets = new uint[num_taps_];
		double *temp_gains = new double[num_taps_];
		for (int i = 0; i < num_taps_; ++i) {
			temp_offsets[i] = tap_array.get_offset_at(i);
			temp_gains[i] = tap_array.get_gain_at(i);
		}
		
		offsets_.assign(temp_offsets, temp_offsets+num_taps_);
		gains_.assign(temp_gains, temp_gains+num_taps_);
		
		delete[] temp_offsets;
		delete[] temp_gains;
    }
	
	~AATapArray		()
	{
	}
    
    const AATapArray&	operator= (const AATapArray& tap_array)
	{
		num_taps_ = tap_array.get_num_taps();
		max_offset_ = tap_array.get_max_offset();
		samplerate_ = tap_array.get_samplerate();
		
		uint *temp_offsets = new uint[num_taps_];
		double *temp_gains = new double[num_taps_];
		for (int i = 0; i < num_taps_; ++i) {
			temp_offsets[i] = tap_array.get_offset_at(i);
			temp_gains[i] = tap_array.get_gain_at(i);
		}
		
		offsets_.assign(temp_offsets, temp_offsets+num_taps_);
		gains_.assign(temp_gains, temp_gains+num_taps_);
		
		delete[] temp_offsets;
		delete[] temp_gains;
		
        return *this;
	}
	
	/* single tap operations
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	void add_tap (double dTime, double dGain)
	{
		if (num_taps_+1 > MAX_NUM_TAPS)
			return;
		
		dTime = (dTime < AADelay::MIN_DELAY_TIME ? AADelay::MIN_DELAY_TIME :
				 (dTime > AADelay::MAX_DELAY_TIME ? AADelay::MAX_DELAY_TIME : dTime));
		dGain = (dGain < AADelay::MIN_DELAY_GAIN ? AADelay::MIN_DELAY_GAIN :
				 (dGain > AADelay::MAX_DELAY_GAIN ? AADelay::MAX_DELAY_GAIN : dGain));
		
        uint temp_offset = static_cast<uint>(dTime*samplerate_);
        
        if (temp_offset > max_offset_) {	// new tap time is the longest one
            
            int offset_diff = temp_offset - max_offset_;
            
            for (int i = 0; i < num_taps_; ++i)
                offsets_[i] += offset_diff;
            
            offsets_.push_back(0);
            gains_.push_back(dGain);
            ++num_taps_;
            max_offset_ = temp_offset;
        }
        else {
            
            offsets_.push_back(max_offset_ - temp_offset);
            gains_.push_back(dGain);
            ++num_taps_;
            
        }
	}
	
	void modify_tap_time (const uint tap, double dTime)
	{
		if (tap >= num_taps_)
			return;
		
		dTime = (dTime < AADelay::MIN_DELAY_TIME ? AADelay::MIN_DELAY_TIME :
				 (dTime > AADelay::MAX_DELAY_TIME ? AADelay::MAX_DELAY_TIME : dTime));
		
		uint temp_offset = static_cast<uint>(dTime*samplerate_);
        
        if (temp_offset > max_offset_) {	// new tap time is the longest one
            
            int offset_diff = temp_offset - max_offset_;
            
            for (int i = 0; i < num_taps_; ++i)
                offsets_[i] += offset_diff;
            
            offsets_[tap] = 0;
            max_offset_ = temp_offset;
        }
        else {	// don't need to adjust max_offset, so just adjust new tap time
            
            offsets_[tap] = max_offset_ - temp_offset;
            
        }
	}
	
	void modify_tap_gain (const uint tap, double dGain)
	{
		if (tap >= num_taps_)
			return;
		
		gains_[tap] = (dGain < AADelay::MIN_DELAY_GAIN ? AADelay::MIN_DELAY_GAIN :
					  (dGain > AADelay::MAX_DELAY_GAIN ? AADelay::MAX_DELAY_GAIN : dGain));
	}
    
    void delete_tap  (const uint tap)
    {
		if (tap >= num_taps_)
			return;
		
		uint temp_offset = offsets_[tap];
		uint temp_max_offset = max_offset_;
		
		offsets_.erase(offsets_.begin()+tap);
		gains_.erase(gains_.begin()+tap);
		--num_taps_;
		
		if (temp_offset == 0) { // 0 is offset for longest tap time
			
			for (int i = 0; i < num_taps_; ++i)	// find least offset, which will be new max
				temp_max_offset = (temp_max_offset > offsets_[i] ? offsets_[i] : temp_max_offset);
			
			for (int j = 0; j < num_taps_; ++j)
				offsets_[j] -= temp_max_offset;
			
			max_offset_ -= temp_max_offset;
		}
    }
	
	/* get functions
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	uint get_offset_at	(const uint tap) const
	{
		assert(tap >= 0 && tap < num_taps_);
		return offsets_[tap];
	}
	
	double get_gain_at (const uint tap) const
	{
		assert(tap >= 0 && tap < num_taps_);
		return gains_[tap];
	}
    
    uint get_num_taps   () const
    {
        return static_cast<uint>(offsets_.size());
    }
    
    uint get_max_offset ()  const
    {
        return max_offset_;
    }
	
	float get_samplerate ()	const
	{
		return samplerate_;
	}
	
	/* utility functions
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	void set_samplerate (float sr)
    {
        if (sr == samplerate_)
			return;
		
		sr = (sr < MIN_SAMPLE_RATE ? MIN_SAMPLE_RATE :
			  (sr > MAX_SAMPLE_RATE ? MAX_SAMPLE_RATE : sr));
		
		double factor = sr / samplerate_;
		
		for (int i = 0; i < num_taps_; ++i)
			offsets_[i] = static_cast<uint>(factor * offsets_[i]);
		
		max_offset_ =  static_cast<uint>(factor * max_offset_);
    }
    
    void print_taps ()
    {
        for (int i = 0; i < num_taps_; ++i)
            std::cout << i << ": " << offsets_[i] << "\t" << gains_[i] << std::endl;
        
    }
	
private:
	
    std::vector<uint> offsets_;
    std::vector<double> gains_;
    
	uint	num_taps_,			// total number of taps
            max_offset_;		// maximum offset value, needed to determine size of AACBuffer
	float	samplerate_;		// sampling rate
	
	void validate_delay_times (double *dTimes)
	{
		for (int i = 0; i < num_taps_; ++i)
			dTimes[i] = (dTimes[i] < AADelay::MIN_DELAY_TIME ? AADelay::MIN_DELAY_TIME :
						 (dTimes[i] > AADelay::MAX_DELAY_TIME ? AADelay::MAX_DELAY_TIME : dTimes[i]));
	}
	
	void validate_delay_gains (double *dGains)
	{
		for (int i = 0; i < num_taps_; ++i)
			dGains[i] = (dGains[i] < AADelay::MIN_DELAY_GAIN ? AADelay::MIN_DELAY_GAIN :
						 (dGains[i] > AADelay::MAX_DELAY_GAIN ? AADelay::MAX_DELAY_GAIN : dGains[i]));
	}
	
};


#endif
