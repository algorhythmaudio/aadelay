/*
 *	File:		main.cpp
 *
 *	Test file for AADelays class.
 *	See AADelays.h for usage and documentation.
 *
 */

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "sndfile.h"

// AA libraries
#include "AADsp.h"
#include "AADelay.h"
#include "AADelayLine.h"
#include "AATapDelay.h"
#include "AACombFilter.h"
#include "AAAllpassFilter.h"

#define VECTORSIZE 8192


int main (int argc, char * const argv[])
{
	SNDFILE *in, *out;
	SF_INFO soundfileInfo;
	AADsp::ChanMode channelMode;
	
	clock_t starttime, endtime;
	
	float *buffer32;							// 32-bit soundfile buffer
	double *buffer64;							// 64-bit soundfile buffer
	float *outbuffer32;							// 32-bit output
	double *outbuffer64;						// 64-bit output
	unsigned int SR, readCount, writeCount;
	
	double tap_times[] = { 0.12, 0.21, 0.32 };
	double tap_gains[] = { 0.9, 0.67, 0.31 };
	
	printf("AADelay test---\n\n");
	
	// open soundfiles
	if (!(in = sf_open(argv[1], SFM_READ, &soundfileInfo))) {
		printf("Could not open %s\n", argv[1]);
		exit(-1);
	}
	
	// validate soundfile properties
	if (soundfileInfo.channels > 2 || soundfileInfo.channels < 1) {
		printf("Error: %s must be either a mono or stereo file.\n", argv[1]);
		sf_close(in);
		exit(-1);
	}
	
	if (!(out = sf_open(argv[2], SFM_WRITE, &soundfileInfo))) {
		printf("Could not open %s\n", argv[2]);
		exit(-1);
	}
	
	// -- initialize buffers and set up parameters --
	
	SR = soundfileInfo.samplerate;
	channelMode = soundfileInfo.channels == 1 ? AADsp::CHAN_MONO : AADsp::CHAN_STEREO;
    
	// basic delay
    AADelayLine basicDelay(0.24,		// delay time in seconds
						   0.61,		// gain
						   0.33,	// feedback
						   SR);		// samplerate
	
	// tap delay
	AATapArray tapArray(3,				// number of taps
						tap_times,		// array of tap times
						tap_gains,		// array of tap gains
						SR);			// samplerate
	tapArray.print_taps();
	std::cout << "max_offset: " << tapArray.get_max_offset() << std::endl;
	std::cout << "num_taps: " << tapArray.get_num_taps() << std::endl;
	
	tapArray.modify_tap_gain(2,		// which tap # (between 0 and number of taps-1)
							 0.11);	// new gain
	tapArray.modify_tap_time(1,		// which tap #
							 0.40);	// new time in seconds
	tapArray.add_tap(0.51,			// tap time in seconds
					 0.58);			// tap gain
	tapArray.delete_tap(3);			// which tap #
	tapArray.print_taps();
	
	AATapDelay tapDelay(tapArray);	// create tap delay with object tapArray of type AATapArray
	std::cout << "max_offset: " << tapArray.get_max_offset() << std::endl;
	std::cout << "num_taps: " << tapArray.get_num_taps() << std::endl;
	
	AACombFilter combFilter(0.03, 1.0, SR);
	
	AAAllpassFilter allpassFilter(0.058, 0.48, SR);
	
	buffer32 = new float[VECTORSIZE*channelMode];
	buffer64 = new double[VECTORSIZE*channelMode];
	outbuffer32 = new float[VECTORSIZE*channelMode];
	outbuffer64 = new double[VECTORSIZE*channelMode];
	
	// -- MAIN LOOP --
	
	printf("Processing...\n");
    
	
	starttime = clock();
	
	do {
		
		readCount = (int)sf_readf_float(in, buffer32, VECTORSIZE);
		//readCount = (int)sf_readf_double(in, buffer64, VECTORSIZE);
		
		// make sure to send a 0-filled buffer to output to avoid unpredictability
		//	in accumulating process
		//memset(outbuffer32, 0, sizeof(float)*VECTORSIZE*channelMode);
		//memset(outbuffer64, 0, sizeof(double)*VECTORSIZE*channelMode);
		
		//basicDelay.process(buffer64, buffer64, readCount);
		//basicDelay.process(buffer32, buffer32, readCount);
		tapDelay.process(buffer32, buffer32, readCount);
		//combFilter.process(buffer32, buffer32, readCount);
		//allpassFilter.process(buffer32, buffer32, readCount);
		
		writeCount = (int)sf_writef_float(out, buffer32, readCount);
		//writeCount = (int)sf_writef_double(out, buffer64, readCount);
		
	} while (readCount);
	
	// flush the delay buffer
	//int flushSize = basicDelay.flush(outbuffer32);
	int flushSize = tapDelay.flush(outbuffer32);
	writeCount = (int)sf_writef_float(out, outbuffer32, flushSize);
	
	endtime = clock();
	printf("Elapsed time: %f secs\n", (endtime - starttime) / (double)CLOCKS_PER_SEC);
	
	
	delete[] buffer32;
	delete[] buffer64;
	delete[] outbuffer32;
	delete[] outbuffer64;
	sf_close(in);
	sf_close(out);
	
	
    return 0;
}

