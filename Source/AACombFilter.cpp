/*
 *	File:		AACombFilter.cpp
 *
 *	Version:	1.0
 *
 *	Created:	12-10-24 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Class definition of the comb filter.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AACombFilter.h"

// for the low-pass filter
#include "AAFilter.cpp"
#include "AAIIRFilter.cpp"


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::AACombFilter default constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AACombFilter::AACombFilter	() : aAdelayLpFilter_(FILTER_LP, 1)
{
    aAdelayGain_ = 0;
	aAdelayLpFilterParams_.set(paramID_Freq, DEFAULT_LPFILTER_FREQ);
	aAdelayLpFilter_.setParameters(aAdelayLpFilterParams_);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::AACombFilter parameter constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AACombFilter::AACombFilter	(double dTime, double rvt, const float sr) : aAdelayLpFilter_(FILTER_LP, 1)
{
	dTime = (dTime < MIN_DELAY_TIME ? MIN_DELAY_TIME :
			 (dTime > MAX_DELAY_TIME ? MAX_DELAY_TIME : dTime));
	rvt = (rvt < MIN_DELAY_RVT ? MIN_DELAY_RVT :
		   (rvt > MAX_DELAY_RVT ? MAX_DELAY_RVT : rvt));
	
    setCombFilterGain(dTime, rvt);
	aAdelayLpFilterParams_.set(paramID_Freq, DEFAULT_LPFILTER_FREQ);
	aAdelayLpFilter_.setParameters(aAdelayLpFilterParams_);
	
	aAdelayBuffer_.resize(static_cast<uint>(dTime * sr));
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::~AACombFilter destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AACombFilter::~AACombFilter  ()
{
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::setCombFilterGain
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AACombFilter::setCombFilterGain   (double dTime, double rvt, const float sr)
{
	dTime = (dTime < MIN_DELAY_TIME ? MIN_DELAY_TIME :
			 (dTime > MAX_DELAY_TIME ? MAX_DELAY_TIME : dTime));
	rvt = (rvt < MIN_DELAY_RVT ? MIN_DELAY_RVT :
		   (rvt > MAX_DELAY_RVT ? MAX_DELAY_RVT : rvt));
	
	aAdelayGain_ = pow(0.001, (dTime/rvt));
	aAdelayBuffer_.resize(static_cast<uint>(dTime * sr));
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::setCombFilterLpFreq
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AACombFilter::setCombFilterLpFreq	(const double lpFreq)
{
	aAdelayLpFilterParams_.set(paramID_Freq, lpFreq);
	aAdelayLpFilter_.setParameters(aAdelayLpFilterParams_);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::flush (32-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AACombFilter::flush		(float *output)
{
    int i;
	
	for (i = 0; i < aAdelayBuffer_.getsize(); ++i) {
		output[i] = aAdelayBuffer_.read();
		aAdelayBuffer_.write(0.);
	}
	
	return aAdelayBuffer_.getsize();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::flush (64-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AACombFilter::flush		(double *output)
{
    int i;
	
	for (i = 0; i < aAdelayBuffer_.getsize(); ++i) {
		output[i] = aAdelayBuffer_.read();
		aAdelayBuffer_.write(0.);
	}
	
	return aAdelayBuffer_.getsize();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::process (32-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AACombFilter::process (float *input, float *output, const uint samples)
{
	float delay_val, lp_val;
	
	for (int i = 0; i < samples; ++i) {
		
		delay_val = aAdelayBuffer_.read();
		
		// low-pass filter the feedback portion
		lp_val = delay_val * aAdelayGain_;
		aAdelayLpFilter_.process(&lp_val, &lp_val, 1);
		
		aAdelayBuffer_.write(input[i] + lp_val);
		output[i] += delay_val;
		
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AACombFilter::process (64-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AACombFilter::process (double *input, double *output, const uint samples)
{
	double delay_val, lp_val;
	
	for (int i = 0; i < samples; ++i) {
		
		delay_val = aAdelayBuffer_.read();
		
		// low-pass filter the feedback portion
		lp_val = delay_val * aAdelayGain_;
		aAdelayLpFilter_.process(&lp_val, &lp_val, 1);
		
		aAdelayBuffer_.write(input[i] + lp_val);
		output[i] += delay_val;
		
	}
}

