/*
 *	File:		AATapDelay.h
 *
 *	Version:	1.0
 *
 *	Created:	12-10-22 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#include "AADelay.h"
#include "AATapArray.h"

#ifndef _AATapDelay_h_
#define _AATapDelay_h_


using namespace AADelay;


#pragma mark ____AATapDelay Declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	tap delay
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class AATapDelay : public AADelayBase {
    
private:
    
    AATapArray	aAdelayTapArray_;		// contains the data on each tap
	double		aAdelayFeedback_;       // feedback value of the delay
    
public:
	
	AATapDelay		();
	AATapDelay		(const AATapArray& tapArray, const double dFeedback=0.);
	~AATapDelay		();
	
	void	setTapArray			(const AATapArray& tapArray);
	void	setDelayFeedback	(const double dFeedback);
	
	uint	getDelaySize		()		const		{ return aAdelayBuffer_.getsize(); }
	double	getDelayFeedback	()		const		{ return aAdelayFeedback_; }
	
	// fills output with the delay tail and returns the length (number of samples) of the tail
	uint	flush				(float *output);
	uint	flush				(double *output);
    
	void	process				(float *input, float *output, const uint samples);
	void	process				(double *input, double *output, const uint samples);
	
};		// AADelayLine


#endif // _AATapDelay_h_
