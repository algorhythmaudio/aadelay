/*
 *	File:		AAAllpassFilter.cpp
 *
 *	Version:	1.0
 *
 *	Created:	12-10-25 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Class definition of the all-pass filter.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AAAllpassFilter.h"


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::AAAllpassFilter default constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AAAllpassFilter::AAAllpassFilter	()
{
    aAdelayGain_ = 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::AAAllpassFilter parameter constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AAAllpassFilter::AAAllpassFilter	(double dTime, double rvt, const float sr)
{
	dTime = (dTime < MIN_DELAY_TIME ? MIN_DELAY_TIME :
			 (dTime > MAX_DELAY_TIME ? MAX_DELAY_TIME : dTime));
	rvt = (rvt < MIN_DELAY_RVT ? MIN_DELAY_RVT :
		   (rvt > MAX_DELAY_RVT ? MAX_DELAY_RVT : rvt));
	
    setAllpassFilterGain(dTime, rvt);
	
	aAdelayBuffer_.resize(static_cast<uint>(dTime * sr));
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::~AAAllpassFilter destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AAAllpassFilter::~AAAllpassFilter  ()
{
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::setCombFilterGain
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AAAllpassFilter::setAllpassFilterGain   (double dTime, double rvt, const float sr)
{
	dTime = (dTime < MIN_DELAY_TIME ? MIN_DELAY_TIME :
			 (dTime > MAX_DELAY_TIME ? MAX_DELAY_TIME : dTime));
	rvt = (rvt < MIN_DELAY_RVT ? MIN_DELAY_RVT :
		   (rvt > MAX_DELAY_RVT ? MAX_DELAY_RVT : rvt));
	
	aAdelayGain_ = pow(0.001, (dTime/rvt));
	aAdelayBuffer_.resize(static_cast<uint>(dTime * sr));
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::flush (32-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AAAllpassFilter::flush	(float *output)
{
    int i;
	
	for (i = 0; i < aAdelayBuffer_.getsize(); ++i) {
		output[i] = aAdelayBuffer_.read();
		aAdelayBuffer_.write(0.);
	}
	
	return aAdelayBuffer_.getsize();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::flush (64-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AAAllpassFilter::flush	(double *output)
{
    int i;
	
	for (i = 0; i < aAdelayBuffer_.getsize(); ++i) {
		output[i] = aAdelayBuffer_.read();
		aAdelayBuffer_.write(0.);
	}
	
	return aAdelayBuffer_.getsize();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::process (32-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AAAllpassFilter::process (float *input, float *output, const uint samples)
{
	float delay_val;
	
	for (int i = 0; i < samples; ++i) {
		
		delay_val = aAdelayBuffer_.read();
		aAdelayBuffer_.write(input[i] + delay_val*aAdelayGain_);
		output[i] += delay_val - aAdelayGain_*input[i];
		
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter::process (64-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AAAllpassFilter::process (double *input, double *output, const uint samples)
{
	double delay_val;
	
	for (int i = 0; i < samples; ++i) {
		
		delay_val = aAdelayBuffer_.read();
		aAdelayBuffer_.write(input[i] + delay_val*aAdelayGain_);
		output[i] += delay_val - aAdelayGain_*input[i];
		
	}
}

