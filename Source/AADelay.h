/*
 *	File:		AADelay.h
 *
 *	Version:	1.0
 *
 *	Created:	12-10-20 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	AADelayBase is a superclass of all defined delays
 *	Delays implemented include:
 *		- generic delay (delay line) class
 *		- tap delay
 *		- comb filter
 *		- all-pass filter
 *
 *  Must include this header file, along with any additional AA delay headers for
 *  filter types to be used.
 *
 *	@usage:--
 *		AADelayLine basicDelay(time, gain, feedback, sr);
 *			time is the delay time in seconds.
 *			gain is the delay gain (default is 1 for no gain).
 *			feedback specifies feedback level (default is 0 for no feedback).
 *			sr is the sampling rate.
 *
 *      AATapDelay tapDelay(tapArray);
 *          tapArray is a AATapArray object containing all the desired taps.
 *		AATapArray tapArray(taps, times[], gains[], sr);
 *			taps is the number of taps in the two arrays times[] and gains[]
 *			where times[] is an array of delay time values and gains[] is the
 *			corresponding array of gain values for each tap.
 *			see AATapArray.h for more usage details.
 *
 *		AACombFilter combFilter(time, rvt, sr);
 *			time is the delay time in seconds (for comb filter, should be >= 10ms to <= 80ms or so).
 *			rvt is the reverb time.
 *
 *		AAAllpassFilter allpassFilter(time, rvt, sr);
 *			same as above.
 *
 *		Supports single channel processing. For multiple channels, de-interleave
 *		audio buffer and apply filter to each channel before interleaving and
 *		writing buffer to output.
 *
 *		Process is accumulating.  If input buffer = output buffer, the delay will be mixed with
 *		original audio.  To isolate delay effect, use a separate, 0-filled buffer as output.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#include <sys/types.h>
#include <cstdlib>
#include <cstring>
#include <cassert>

// AA libraries
#include "AADsp.h"
#include "AACBuffer.h"

#ifndef _AADelay_h_
#define _AADelay_h_

using namespace AADsp;


#pragma mark ____AADelay namespace
namespace AADelay {
	
// limit defines
const double MIN_DELAY_TIME			= 0.l;		// minimum delay time, in seconds
const double MAX_DELAY_TIME			= 5.l;		// maximum delay time, in seconds
const double MIN_DELAY_GAIN			= 0.l;
const double MAX_DELAY_GAIN			= 10.l;
const double MIN_DELAY_FEEDBACK		= 0.l;
const double MAX_DELAY_FEEDBACK		= 2.l;
const double MIN_DELAY_RVT			= 0.001;	// minimum delay reverb time, in seconds
const double MAX_DELAY_RVT			= 5.;		// maximum delay reverb time, in seconds
	
	
#pragma mark ____AADelayBase Declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayBase class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	delay base class, pure virtual
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
class AADelayBase {
        
protected:
	
	AACBuffer<double> aAdelayBuffer_;
        
public:
	
	virtual ~AADelayBase		();
	
	/* 32-bit & 64-bit process functions
	 */
	virtual void	process		(float *input,			// input buffer
								float *output,			// output buffer
								const uint samples) = 0;	// number of samples to process
	virtual void	process		(double *input,
								 double *output,
								 const uint samples) = 0;
    
        
};		// AADelayBase
	
}		// AADelay namespace


#endif	// _AADelay_h_

