/*
 *	File:		AADelayLine.h
 *
 *	Version:	1.0
 *
 *	Created:	12-10-20 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */


#include "AADelay.h"

#ifndef _AADelayLine_h_
#define _AADelayLine_h_

using namespace AADelay;


#pragma mark ____AADelayLine Declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
class AADelayLine : public AADelayBase {
        
private:
		
	double	aAdelayGain_,
			aAdelayFeedback_;
        
public:
	
	AADelayLine		();
	AADelayLine		(const double dTime, const double dGain=1., const double dFeedback=0., const float sr=DEFAULT_SAMPLE_RATE);
	~AADelayLine	();
	
	void	setDelayTime		(const double dTime, const float sr=DEFAULT_SAMPLE_RATE);
	void	setDelayGain		(const double dGain);
	void	setDelayFeedback	(const double dFeedback);
	
	uint	getDelaySize		()		const		{ return aAdelayBuffer_.getsize(); }
	double	getDelayGain		()		const		{ return aAdelayGain_; }
	double	getDelayFeedback	()		const		{ return aAdelayFeedback_; }
	
	// fills output with the delay tail and returns the length (number of samples) of the tail
	uint	flush				(float *output);
	uint	flush				(double *output);
		
	void	process				(float *input, float *output, const uint samples);
	void	process				(double *input, double *output, const uint samples);
	
};		// AADelayLine


#endif // _AADelayLine_h_
