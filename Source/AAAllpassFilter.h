/*
 *	File:		AAAllpassFilter.h
 *
 *	Version:	1.0
 *
 *	Created:	12-10-25 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#include "AADelay.h"

#ifndef _AAAllpassFilter_h_
#define _AAAllpassFilter_h_

using namespace AADelay;


#pragma mark ____AAAllpassFilter Declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAAllpassFilter class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	Feedback/feedforward all-pass filter
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class AAAllpassFilter : public AADelayBase {
    
private:
    
    double      aAdelayGain_;
    
public:
	
	AAAllpassFilter         ();
	AAAllpassFilter         (double dTime, double rvt, const float sr=DEFAULT_SAMPLE_RATE);
	~AAAllpassFilter        ();
	
	void	setAllpassFilterGain    (double dTime, double rvt, const float sr=DEFAULT_SAMPLE_RATE);
	
	double	getAllpassFilterGain	()		const		{ return aAdelayGain_; }
	
	// fills output with the delay tail and returns the length (number of samples) of the tail
	uint	flush				(float *output);
	uint	flush				(double *output);
    
	void	process				(float *input, float *output, const uint samples);
	void	process				(double *input, double *output, const uint samples);
	
};		// AAAllpassFilter


#endif // _AAAllpassFilter_h_

