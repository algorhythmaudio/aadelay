/*
 *	File:		AADelay.cpp
 *
 *	Version:	1.0
 *
 *	Created:	12-10-20 by Christian Floisand
 *	Updated:	12-10-21 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Class definition of abstract base delay superclass.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AADelay.h"


using namespace AADelay;

AADelayBase::~AADelayBase ()
{
}
