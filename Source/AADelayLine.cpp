/*
 *	File:		AADelayLine.cpp
 *
 *	Version:	1.0
 *
 *	Created:	12-10-20 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Class definition of delay line.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AADelayLine.h"


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::AADelayLine default constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AADelayLine::AADelayLine	()
{
	aAdelayGain_ = 1.;
	aAdelayFeedback_ = 0.;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::AADelayLine parameter constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AADelayLine::AADelayLine		(const double dTime, const double dGain, const double dFeedback, const float sr)
{
	aAdelayGain_ = (dGain < MIN_DELAY_GAIN ? MIN_DELAY_GAIN :
					(dGain > MAX_DELAY_GAIN ? MAX_DELAY_GAIN : dGain));
	aAdelayFeedback_ = (dFeedback < MIN_DELAY_FEEDBACK ? MIN_DELAY_FEEDBACK :
						(dFeedback > MAX_DELAY_FEEDBACK ? MAX_DELAY_FEEDBACK : dFeedback));
	
	aAdelayBuffer_.resize(static_cast<uint>(dTime * sr)); // need rounding on size? add 0.5?
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::~AADelayLine destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AADelayLine::~AADelayLine	()
{
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::setDelayTime
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AADelayLine::setDelayTime		(const double dTime, const float sr)
{
	if ((dTime*sr) == aAdelayBuffer_.getsize())
		return;
	
	double temp_dTime = (dTime < MIN_DELAY_TIME ? MIN_DELAY_TIME :
						 (dTime > MAX_DELAY_TIME ? MAX_DELAY_TIME : dTime));
	
	aAdelayBuffer_.resize(static_cast<uint>(temp_dTime * sr));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::setDelayGain
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AADelayLine::setDelayGain		(const double dGain)
{
	if (dGain == aAdelayGain_)
		return;
	
	aAdelayGain_ = (dGain < MIN_DELAY_GAIN ? MIN_DELAY_GAIN :
					(dGain > MAX_DELAY_GAIN ? MAX_DELAY_GAIN : dGain));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::setDelayFeedback
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AADelayLine::setDelayFeedback	(const double dFeedback)
{
	if (dFeedback == aAdelayFeedback_)
		return;
	
	aAdelayFeedback_ = (dFeedback < MIN_DELAY_FEEDBACK ? MIN_DELAY_FEEDBACK :
						(dFeedback > MAX_DELAY_FEEDBACK ? MAX_DELAY_FEEDBACK : dFeedback));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::flush (32-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AADelayLine::flush	(float *output)
{
	int i;
	
	for (i = 0; i < aAdelayBuffer_.getsize(); ++i) {
		output[i] = aAdelayBuffer_.read() * aAdelayGain_;
		aAdelayBuffer_.write(0.);
	}
	
	return aAdelayBuffer_.getsize();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::flush (64-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AADelayLine::flush	(double *output)
{
	int i;
	
	for (i = 0; i < aAdelayBuffer_.getsize(); ++i) {
		output[i] = aAdelayBuffer_.read() * aAdelayGain_;
		aAdelayBuffer_.write(0.);
	}
	
	return aAdelayBuffer_.getsize();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::process (32-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AADelayLine::process (float *input, float *output, const uint samples)
{
	int i;
	float delay_val;
	
	for (i = 0; i < samples; ++i) {
		delay_val = aAdelayBuffer_.read();
		aAdelayBuffer_.write(input[i] + (delay_val*aAdelayFeedback_));
		output[i] += delay_val * aAdelayGain_;
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AADelayLine::process (64-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AADelayLine::process (double *input, double *output, const uint samples)
{
	int i;
	double delay_val;
	
	for (i = 0; i < samples; ++i) {
		delay_val = aAdelayBuffer_.read();
		aAdelayBuffer_.write(input[i] + (delay_val*aAdelayFeedback_));
		output[i] += delay_val * aAdelayGain_;
	}
}

