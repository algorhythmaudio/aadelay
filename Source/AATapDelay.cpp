/*
 *	File:		AATapDelay.cpp
 *
 *	Version:	1.0
 *
 *	Created:	12-10-22 by Christian Floisand
 *	Updated:	12-10-29 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Class definition of the tap delay.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AATapDelay.h"


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::AATapDelay default constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AATapDelay::AATapDelay		()
{
	aAdelayFeedback_ = 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::AATapDelay parameter constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AATapDelay::AATapDelay		(const AATapArray& tapArray, const double dFeedback)
{
	aAdelayTapArray_ = tapArray;
	aAdelayFeedback_ = (dFeedback < MIN_DELAY_FEEDBACK ? MIN_DELAY_FEEDBACK :
						(dFeedback > MAX_DELAY_FEEDBACK ? MAX_DELAY_FEEDBACK : dFeedback));
	
	aAdelayBuffer_.resize(aAdelayTapArray_.get_max_offset());
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::AATapDelay destructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AATapDelay::~AATapDelay		()
{
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::setTapArray
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AATapDelay::setTapArray		(const AATapArray& tapArray)
{
	aAdelayTapArray_ = tapArray;
	
	aAdelayBuffer_.resize(aAdelayTapArray_.get_max_offset());
	aAdelayBuffer_.clear();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::AATapDelay setDelayFeedback
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void	AATapDelay::setDelayFeedback	(const double dFeedback)
{
	aAdelayFeedback_ = (dFeedback < MIN_DELAY_FEEDBACK ? MIN_DELAY_FEEDBACK :
						(dFeedback > MAX_DELAY_FEEDBACK ? MAX_DELAY_FEEDBACK : dFeedback));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::AATapDelay flush (32-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AATapDelay::flush	(float *output)
{
	int i, j;
	uint delaySize = aAdelayTapArray_.get_max_offset();
	
	for (i = 0; i < delaySize; ++i) {
			
		for (j = 0; j < aAdelayTapArray_.get_num_taps(); ++j)
			output[i] += aAdelayBuffer_.read_noincr(aAdelayTapArray_.get_offset_at(j)) * aAdelayTapArray_.get_gain_at(j);
		
		aAdelayBuffer_.incr_readpos();
		aAdelayBuffer_.write(0.);
	}
	
	return delaySize;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::AATapDelay flush (64-bit)
//	This function should normally only be called in an offline mode since
//	in real time, the host will handle the delay tail.
// Fills output with the delay tail and returns the length in samples of the tail
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uint	AATapDelay::flush	(double *output)
{
	int i, j;
	uint delaySize = aAdelayTapArray_.get_max_offset();
	
	for (i = 0; i < delaySize; ++i) {
		
		for (j = 0; j < aAdelayTapArray_.get_num_taps(); ++j)
			output[i] += aAdelayBuffer_.read_noincr(aAdelayTapArray_.get_offset_at(j)) * aAdelayTapArray_.get_gain_at(j);
		
		aAdelayBuffer_.incr_readpos();
		aAdelayBuffer_.write(0.);
	}
	
	return delaySize;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::process (32-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AATapDelay::process (float *input, float *output, const uint samples)
{
	int i, j;
	float delay_val;
	
	for (i = 0; i < samples; ++i) {
		delay_val = 0.;
		
		for (j = 0; j < aAdelayTapArray_.get_num_taps(); ++j)	// mix taps together
			delay_val += aAdelayBuffer_.read_noincr(aAdelayTapArray_.get_offset_at(j)) * aAdelayTapArray_.get_gain_at(j);
		
		aAdelayBuffer_.incr_readpos();	// read from each tap before incrementing read pointer
		aAdelayBuffer_.write(input[i]+(delay_val*aAdelayFeedback_));
		output[i] += delay_val;
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AATapDelay::process (64-bit processing)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AATapDelay::process (double *input, double *output, const uint samples)
{
	int i, j;
	double delay_val;
	
	for (i = 0; i < samples; ++i) {
		delay_val = 0.;
		
		for (j = 0; j < aAdelayTapArray_.get_num_taps(); ++j)	// mix taps together
			delay_val += aAdelayBuffer_.read_noincr(aAdelayTapArray_.get_offset_at(j)) * aAdelayTapArray_.get_gain_at(j);
		
		aAdelayBuffer_.incr_readpos();	// read from each tap before incrementing read pointer
		aAdelayBuffer_.write(input[i]+(delay_val*aAdelayFeedback_));
		output[i] += delay_val;
	}
}


